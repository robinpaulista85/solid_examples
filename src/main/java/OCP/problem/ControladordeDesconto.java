package OCP.problem;

public class ControladordeDesconto {

    public void adicionaDescontoLivroInfantil(DescontoLivroInfantil descontoLivroInfantil) {
        descontoLivroInfantil.valorDescontoLivroInfantil();
    }

    public void adicionaDescontoLivroInfantil(DescontoLivroInfantil descontoLivroInfantil) {
        descontoLivroAutoAjuda.valorDescontoAutoAjuda();
    }

    public void adicionaDescontoLivroAcao(DescontoLivroAcao descontoLivroAcao){
        descontoLivroAcao.descontoLivroAcao();
    }
}
